#lang racket

(require xml)

(require "f1.rkt" "f2.rkt" "f3.rkt" "my-structures.rkt")

(provide
 (contract-out
  [struct section
    ((type (curryr member (list 'h1 'h2 'h3 'h4 'h5)))
     (key symbol?)
     (title string?)
     (content (listof xexpr?)))]
  (display-all-to-hml (-> string? (listof section?) void?))
  (display-to-hmtl-with-lang (-> (hash/c symbol? xexpr?) void?))
  (create-index (-> xexpr? void?))))

(define display-all-to-hml (compose display-to-hmtl-with-lang f2 f1))

(define (create-index content-xexpr)
  (display-to-hmtl 'index "none" null null content-xexpr))