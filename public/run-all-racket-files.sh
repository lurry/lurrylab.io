
for file in "$PWD"/**/*.rkt
do
  echo $file
  cd "$(dirname $file)" || exit
  racket "$(basename $file)"
done